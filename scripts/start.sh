#!/usr/bin/env bash

cd ../

gradle clean war

docker build --tag veptechno/scheduler:latest .

docker-compose down
docker-compose up