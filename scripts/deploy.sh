#!/usr/bin/env bash

cd ../

gradle clean war

docker build --tag veptechno/scheduler:latest .

docker push veptechno/scheduler:latest

ssh main.vscale '
  docker pull veptechno/scheduler:latest
  cd ~/scheduler
  docker-compose down
  docker-compose up
'