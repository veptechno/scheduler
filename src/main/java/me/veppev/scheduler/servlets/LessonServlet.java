package me.veppev.scheduler.servlets;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import lombok.NonNull;
import me.veppev.scheduler.entities.Lesson;
import me.veppev.scheduler.json.Converter;
import me.veppev.scheduler.services.LessonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;

@WebServlet(name = "LessonServlet", urlPatterns = {"/lesson"})
public class LessonServlet extends HttpServlet {

    private LessonService lessonService;
    private final Logger logger = LoggerFactory.getLogger(LessonServlet.class);

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        lessonService = new LessonService();
        logger.debug("Инициализирован {}", this);
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.addHeader("Access-Control-Allow-Origin", "*");
        resp.addHeader("Access-Control-Allow-Methods",  "*");
        super.service(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("GET запрос пар у {}", this);
        Set<Lesson> lessons = lessonService.findAll();

        String lessonsJson = Converter.toJSON(lessons);
        resp.getWriter().print(lessonsJson);
        resp.setContentType("application/json");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Запрос на добавление новой пары в {}", this);
        try {
            String json = req.getReader().lines().collect(Collectors.joining());
            Lesson lesson = Converter.toJavaObject(json, Lesson.class);
            lessonService.save(lesson);
            logger.info("Добавлена новая пара {}", lesson);
            resp.setStatus(HttpServletResponse.SC_CREATED);
        } catch (JsonParseException | JsonMappingException e) {
            logger.error("Ошибка парсинга пары", e);
            resp.setStatus(HttpServletResponse.SC_NOT_IMPLEMENTED);
        }

    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Запрос на удаление пары в {}", this);
        try {
            int id = Integer.parseInt(req.getParameter("id"));
            @NonNull Lesson lesson = lessonService.findLessonById(id);

            lessonService.delete(lesson);
            logger.info("Пара с id={} удалена", id);
            resp.setStatus(HttpServletResponse.SC_OK);
        } catch (NullPointerException | NumberFormatException e) {
            logger.error("Не удалось найти пару с данным id", e);
            resp.setStatus(HttpServletResponse.SC_NOT_IMPLEMENTED);
        }
    }
}
