package me.veppev.scheduler.servlets;

import me.veppev.scheduler.entities.Group;
import me.veppev.scheduler.entities.Lesson;
import me.veppev.scheduler.entities.Teacher;
import me.veppev.scheduler.json.Converter;
import me.veppev.scheduler.services.GroupService;
import me.veppev.scheduler.services.LessonService;
import me.veppev.scheduler.services.TeacherService;
import me.veppev.scheduler.utils.HibernateSessionFactoryUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

@WebServlet(name = "SchedulerServlet", urlPatterns = {"/timetable"}, loadOnStartup = 1)
public class SchedulerServlet extends HttpServlet {

    private GroupService groupService;
    private TeacherService teacherService;
    private LessonService lessonService;
    private final Logger logger = LoggerFactory.getLogger(SchedulerServlet.class);

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        groupService = new GroupService();
        teacherService = new TeacherService();
        lessonService = new LessonService();

        HibernateSessionFactoryUtil.init();

        logger.debug("Инициализирован {}", this);
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.addHeader("Access-Control-Allow-Origin", "*");
        resp.addHeader("Access-Control-Allow-Methods",  "*");
        resp.setContentType("application/json");
        super.service(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, String[]> parameterMap = req.getParameterMap();
        if (parameterMap.containsKey("group_id")) {
            computeByGroup(req, resp);
        } else if (parameterMap.containsKey("teacher_id")) {
            computeByTeacher(req, resp);
        } else {
            computeAll(req, resp);
        }
    }

    private void computeByGroup(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            int id = Integer.parseInt(req.getParameter("group_id"));
            Group group = groupService.findGroupById(id);
            Set<Lesson> lessonsOfGroup = group.getLessons();

            String json = Converter.toJSON(lessonsOfGroup);
            resp.getWriter().print(json);
            resp.setStatus(HttpServletResponse.SC_OK);
        } catch (NullPointerException | NumberFormatException e) {
            resp.setStatus(HttpServletResponse.SC_NOT_IMPLEMENTED);
        }
    }

    private void computeByTeacher(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            int id = Integer.parseInt(req.getParameter("teacher_id"));
            Teacher teacher = teacherService.findTeacherById(id);
            List<Lesson> lessonsOfTeacher = teacherService.getLessons(teacher);

            String json = Converter.toJSON(lessonsOfTeacher);
            resp.getWriter().print(json);
            resp.setStatus(HttpServletResponse.SC_OK);
        } catch (NullPointerException | NumberFormatException e) {
            resp.setStatus(HttpServletResponse.SC_NOT_IMPLEMENTED);
        }
    }

    private void computeAll(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            Set<Lesson> lessons = lessonService.findAll();

            String json = Converter.toJSON(lessons);
            resp.getWriter().print(json);
            resp.setStatus(HttpServletResponse.SC_OK);
        } catch (NullPointerException | NumberFormatException e) {
            resp.setStatus(HttpServletResponse.SC_NOT_IMPLEMENTED);
        }
    }
}
