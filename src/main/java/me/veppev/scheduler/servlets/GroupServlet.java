package me.veppev.scheduler.servlets;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import lombok.NonNull;
import me.veppev.scheduler.entities.Group;
import me.veppev.scheduler.json.Converter;
import me.veppev.scheduler.services.GroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;

@WebServlet(name = "GroupServlet", urlPatterns = {"/group"})
public class GroupServlet extends HttpServlet {

    private static Logger logger = LoggerFactory.getLogger(GroupServlet.class);
    private GroupService groupService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        groupService = new GroupService();
        logger.debug("Инициализирован {}", this);
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.addHeader("Access-Control-Allow-Origin", "*");
        resp.addHeader("Access-Control-Allow-Methods",  "*");
        super.service(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("GET запрос групп у {}", this);
        Set<Group> groups = groupService.findAll();

        String groupsJson = Converter.toJSON(groups);
        resp.getWriter().print(groupsJson);
        resp.setContentType("application/json");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Запрос на добавление новой группы в {}", this);
        try {
            String json = req.getReader().lines().collect(Collectors.joining());
            logger.debug("json от клиента = {}", json);
            Group group = Converter.toJavaObject(json, Group.class);
            logger.debug("Сформированная группа = {}", group);
            groupService.save(group);
            logger.info("Добавлена новая группа {}", group);
            resp.setStatus(HttpServletResponse.SC_CREATED);
        } catch (JsonParseException | JsonMappingException e) {
            logger.error("Ошибка парсинга группы", e);
            resp.setStatus(HttpServletResponse.SC_NOT_IMPLEMENTED);
        }

    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Запрос на удаление группы в {}", this);
        try {
            int id = Integer.parseInt(req.getParameter("id"));
            @NonNull Group group = groupService.findGroupById(id);

            groupService.delete(group);
            logger.info("Группа с id={} удалена", id);
            resp.setStatus(HttpServletResponse.SC_OK);
        } catch (NullPointerException | NumberFormatException e) {
            logger.error("Не удалось найти группу с данным id", e);
            resp.setStatus(HttpServletResponse.SC_NOT_IMPLEMENTED);
        }
    }
}
