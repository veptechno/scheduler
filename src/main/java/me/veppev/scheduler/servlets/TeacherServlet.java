package me.veppev.scheduler.servlets;

import me.veppev.scheduler.entities.Teacher;
import me.veppev.scheduler.json.Converter;
import me.veppev.scheduler.services.TeacherService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;

@WebServlet(name = "TeacherServlet", urlPatterns = {"/teacher"})
public class TeacherServlet extends HttpServlet {

    private TeacherService teacherService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        teacherService = new TeacherService();
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.addHeader("Access-Control-Allow-Origin", "*");
        resp.addHeader("Access-Control-Allow-Methods",  "*");
        resp.setContentType("application/json");
        super.service(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            int id = Integer.parseInt(req.getParameter("id"));
            Teacher teacher = teacherService.findTeacherById(id);

            String teacherJson = Converter.toJSON(teacher);
            resp.getWriter().print(teacherJson);
        } catch (NullPointerException | NumberFormatException e) {
            Set<Teacher> teachers = teacherService.findAll();

            String teachersJson = Converter.toJSON(teachers);
            resp.getWriter().print(teachersJson);
        }
    }

    //Add new teacher
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String json = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
            Teacher teacher = Converter.toJavaObject(json, Teacher.class);

            teacherService.save(teacher);

            resp.setStatus(HttpServletResponse.SC_CREATED);
        } catch (NullPointerException | NumberFormatException | ArrayIndexOutOfBoundsException e) {
            resp.setStatus(HttpServletResponse.SC_NOT_IMPLEMENTED);
            //resp.sendRedirect("edit/teacher.html");
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            int id = Integer.parseInt(req.getParameter("id"));
            Teacher teacher = teacherService.findTeacherById(id);

            teacherService.delete(teacher);
            resp.setStatus(HttpServletResponse.SC_OK);
        } catch (NullPointerException | NumberFormatException e) {
            resp.setStatus(HttpServletResponse.SC_NOT_IMPLEMENTED);
        }
    }
}
