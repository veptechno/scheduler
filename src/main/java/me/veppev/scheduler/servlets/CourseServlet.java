package me.veppev.scheduler.servlets;

import lombok.NonNull;
import me.veppev.scheduler.entities.Course;
import me.veppev.scheduler.json.Converter;
import me.veppev.scheduler.services.CourseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;

@WebServlet(name = "CourseServlet", urlPatterns = {"/course"})
public class CourseServlet extends HttpServlet {

    private CourseService courseService;
    private final Logger logger = LoggerFactory.getLogger(CourseServlet.class);

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        courseService = new CourseService();
        logger.debug("Инициализирован {}", this);
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.addHeader("Access-Control-Allow-Origin", "*");
        resp.addHeader("Access-Control-Allow-Methods",  "*");
        super.service(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Выполнен запрос GET к {}", this);
        Set<Course> courses = courseService.findAll();

        String coursesJson = Converter.toJSON(courses);
        resp.getWriter().print(coursesJson);
        resp.setContentType("application/json");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Выполнен запрос POST к {}", this);
        String json = req.getReader().lines().collect(Collectors.joining());

        Course newCourse = Converter.toJavaObject(json, Course.class);
        if (newCourse.getTeacher() != null) {
            courseService.save(newCourse);
            logger.info("Создан новый курс {}", newCourse);
            resp.setStatus(HttpServletResponse.SC_CREATED);
        } else {
            logger.warn("Присланный новый курс содержит не валидный teacher_id, json={}", json);
            resp.setStatus(HttpServletResponse.SC_NOT_IMPLEMENTED);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Пришёл запрос на удаление с id={} к {}", req.getParameter("id"), this);

        try {
            int id = Integer.parseInt(req.getParameter("id"));
            @NonNull Course course = courseService.findCourseById(id);

            courseService.delete(course);
            resp.setStatus(HttpServletResponse.SC_OK);
        } catch (NullPointerException | NumberFormatException e) {
            logger.error("Ошибка парсинга id", e);
            resp.setStatus(HttpServletResponse.SC_NOT_IMPLEMENTED);
        }
    }
}
