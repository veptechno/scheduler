package me.veppev.scheduler.servlets;

import me.veppev.scheduler.services.TeacherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "LoginServlet", urlPatterns = {"/login"}, loadOnStartup = 1)
public class LoginServlet extends HttpServlet {

    private TeacherService teacherService;
    private static Logger logger;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        teacherService = new TeacherService();
        logger = LoggerFactory.getLogger(LoginServlet.class);
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.addHeader("Access-Control-Allow-Origin", "*");
        resp.addHeader("Access-Control-Allow-Methods",  "*");
        super.service(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String email = req.getParameter("email");
        String password = req.getParameter("password");

        logger.info("Пришёл запрос на логин email={}, password={}", email, password);

        if(teacherService.teacherExists(email, password)) {
            logger.info("Удачный логин учителя с email={}", email);

            req.getSession().setAttribute("granted", true);
            resp.setStatus(HttpServletResponse.SC_OK);
            resp.sendRedirect("admin/index.html");
        } else {
            logger.warn("Неверный логин или пароль email={} password={}", email, password);

            resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            resp.sendRedirect("login.html");
        }
    }
}
