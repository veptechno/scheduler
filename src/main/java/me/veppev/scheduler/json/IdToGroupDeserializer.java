package me.veppev.scheduler.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import me.veppev.scheduler.entities.Group;
import me.veppev.scheduler.services.GroupService;

import java.io.IOException;

public class IdToGroupDeserializer extends JsonDeserializer<Group> {

    @Override
    public Group deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        GroupService groupService = new GroupService();
        return groupService.findGroupById(p.readValueAs(Integer.class));
    }
}
