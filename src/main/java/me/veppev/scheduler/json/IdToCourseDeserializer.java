package me.veppev.scheduler.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import me.veppev.scheduler.entities.Course;
import me.veppev.scheduler.services.CourseService;

import java.io.IOException;

public class IdToCourseDeserializer extends JsonDeserializer<Course> {

    @Override
    public Course deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        CourseService courseService = new CourseService();
        return courseService.findCourseById(p.readValueAs(Integer.class));
    }
}
