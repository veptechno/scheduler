package me.veppev.scheduler.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import me.veppev.scheduler.entities.Teacher;
import me.veppev.scheduler.services.TeacherService;

import java.io.IOException;

public class IdToTeacherDeserializer extends JsonDeserializer<Teacher> {
    @Override
    public Teacher deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        TeacherService teacherService = new TeacherService();
        int id = p.readValueAs(Integer.class);

        return teacherService.findTeacherById(id);
    }
}
