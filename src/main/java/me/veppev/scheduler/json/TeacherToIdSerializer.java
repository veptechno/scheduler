package me.veppev.scheduler.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import me.veppev.scheduler.entities.Teacher;

import java.io.IOException;

public class TeacherToIdSerializer extends JsonSerializer<Teacher> {

    @Override
    public void serialize(Teacher value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeNumber(value.getId());
    }
}
