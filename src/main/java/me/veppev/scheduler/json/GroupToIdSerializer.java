package me.veppev.scheduler.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import me.veppev.scheduler.entities.Group;

import java.io.IOException;

public class GroupToIdSerializer extends JsonSerializer<Group> {
    @Override
    public void serialize(Group value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeNumber(value.getId());
    }
}
