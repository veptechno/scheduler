package me.veppev.scheduler.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import me.veppev.scheduler.entities.Course;

import java.io.IOException;

public class CourseToIdSerializer extends JsonSerializer<Course> {
    @Override
    public void serialize(Course value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeNumber(value.getId());
    }
}