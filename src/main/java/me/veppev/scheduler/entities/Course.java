package me.veppev.scheduler.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import me.veppev.scheduler.json.IdToTeacherDeserializer;
import me.veppev.scheduler.json.TeacherToIdSerializer;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "course")
@ToString
@EqualsAndHashCode(of = {"id"})
public class Course {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter @Setter
	private int id;

	@Column(name = "name")
	@Getter @Setter
	private String title;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "teacher_id")
	@JsonProperty(value = "teacher_id")
	@JsonSerialize(using = TeacherToIdSerializer.class)
	@JsonDeserialize(using = IdToTeacherDeserializer.class)
	@Getter @Setter
	private Teacher teacher;

	@OneToMany(mappedBy = "course", fetch = FetchType.EAGER)
	@JsonIgnore
	@Getter @Setter
	private Set<Lesson> lessons;
}