package me.veppev.scheduler.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "group_table")
@ToString
@EqualsAndHashCode(of = {"id"})
public class Group {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter @Setter
	private int id;

	@Column(name = "number", unique = true)
	@Getter @Setter
	private String number;

	@Column(name = "year")
	@Getter @Setter
	private int year;

	@OneToMany(mappedBy = "group", fetch = FetchType.EAGER)
	@Getter @Setter
	private Set<Lesson> lessons;
}