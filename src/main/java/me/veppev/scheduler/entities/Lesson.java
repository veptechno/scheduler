package me.veppev.scheduler.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import me.veppev.scheduler.json.CourseToIdSerializer;
import me.veppev.scheduler.json.GroupToIdSerializer;
import me.veppev.scheduler.json.IdToCourseDeserializer;
import me.veppev.scheduler.json.IdToGroupDeserializer;

import javax.persistence.*;
import java.time.LocalTime;
import java.util.Objects;

@Entity
@Table(name = "lesson")
@EqualsAndHashCode(of = {"id"})
public class Lesson {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter @Setter
	private int id;

	@Column(name = "time")
	@JsonFormat(pattern = "HH:mm")
	@Getter @Setter
	private LocalTime time;

	@Column
	@Getter @Setter
	private Integer day;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "group_id")
	@JsonProperty(value = "group_id")
	@JsonSerialize(using = GroupToIdSerializer.class)
	@JsonDeserialize(using = IdToGroupDeserializer.class)
	@Getter @Setter
	private Group group;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "course_id")
	@JsonProperty(value = "course_id")
	@JsonSerialize(using = CourseToIdSerializer.class)
	@JsonDeserialize(using = IdToCourseDeserializer.class)
	@Getter @Setter
	private Course course;
}