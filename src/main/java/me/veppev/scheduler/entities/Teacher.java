package me.veppev.scheduler.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "teacher")
@EqualsAndHashCode(of = {"id"})
public class Teacher {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter @Setter
	private int id;

	@Column(name = "first_name")
	@Getter @Setter
	private String firstName;

	@Column(name = "second_name")
	@Getter @Setter
	private String secondName;

	@Column(name = "salary")
	@Getter @Setter
	private int salary;

	@Column(name = "birth_date")
	@Getter @Setter
	private LocalDate birthDate;

	@Column(name = "email", unique = true)
	@Getter @Setter
	private String email;

	@Column(name = "password")
	@Getter @Setter
	private String password;

	@OneToMany(mappedBy = "teacher", fetch = FetchType.EAGER)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnore
	@Getter @Setter
	private Set<Course> courses;
}