package me.veppev.scheduler.dao;

import me.veppev.scheduler.entities.Course;

import java.util.Set;

public interface CourseDao {

    Course findById(int id);

    Set<Course> findAll();

    void save(Course course);

    void delete(Course course);

}
