package me.veppev.scheduler.dao;

import me.veppev.scheduler.entities.Lesson;
import me.veppev.scheduler.utils.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class LessonDaoImpl implements LessonDao {
    @Override
    public void save(Lesson lesson) {
        try (Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession()) {
            Transaction tx1 = session.beginTransaction();
            session.save(lesson);
            tx1.commit();
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public Set<Lesson> findAll() {
        try (Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession()) {
            return new HashSet<>((List<Lesson>) session.createQuery("From Lesson").list());
        }
    }

    @Override
    public Lesson findById(int id) {
        try (Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession()) {
            return session.get(Lesson.class, id);
        }
    }

    @Override
    public void delete(Lesson lesson) {
        try (Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession()) {
            Transaction tx1 = session.beginTransaction();
            session.delete(lesson);
            tx1.commit();
        }
    }
}
