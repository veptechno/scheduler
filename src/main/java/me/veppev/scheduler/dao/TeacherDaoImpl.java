package me.veppev.scheduler.dao;

import me.veppev.scheduler.entities.Teacher;
import me.veppev.scheduler.utils.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TeacherDaoImpl implements TeacherDao {
    @Override
    public Teacher findById(int id) {
        try (Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession()) {
            return session.get(Teacher.class, id);
        }
    }

    @Override
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public Teacher findByEmail(String email) {
        try (Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession()) {
            Query query = session.createQuery("From Teacher where email = :email");
            query.setParameter("email", email);
            List<Teacher> teachers = (List<Teacher>) query.list();

            return teachers.isEmpty() ? null : teachers.get(0);
        }
    }

    @Override
    public void save(Teacher teacher) {
        try (Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession()) {
            Transaction tx1 = session.beginTransaction();
            session.save(teacher);
            tx1.commit();
        }
    }

    @Override
    public void update(Teacher teacher) {
        try (Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession()) {
            Transaction tx1 = session.beginTransaction();
            session.update(teacher);
            tx1.commit();
        }
    }

    @Override
    public void delete(Teacher teacher) {
        try (Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession()) {
            Transaction tx1 = session.beginTransaction();
            session.delete(teacher);
            tx1.commit();
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public Set<Teacher> findAll() {
        try (Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession()) {
            return new HashSet<>((List<Teacher>) session.createQuery("From Teacher").list());
        }
    }
}
