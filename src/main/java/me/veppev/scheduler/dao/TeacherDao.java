package me.veppev.scheduler.dao;

import me.veppev.scheduler.entities.Teacher;

import java.util.Set;

public interface TeacherDao {

    Teacher findById(int id);

    Teacher findByEmail(String email);

    void save(Teacher teacher);

    void update(Teacher teacher);

    void delete(Teacher teacher);

    Set<Teacher> findAll();

}
