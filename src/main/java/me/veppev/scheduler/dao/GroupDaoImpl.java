package me.veppev.scheduler.dao;

import me.veppev.scheduler.entities.Group;
import me.veppev.scheduler.utils.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GroupDaoImpl implements GroupDao {
    @Override
    public Group findById(int id) {
        try (Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession()) {
            return session.get(Group.class, id);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public Set<Group> findAll() {
        try (Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession()) {
            return new HashSet<>((List<Group>) session.createQuery("From Group").list());
        }
    }

    @Override
    public void save(Group group) {
        try (Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession()) {
            Transaction tx1 = session.beginTransaction();
            session.save(group);
            tx1.commit();
        }
    }

    @Override
    public void delete(Group group) {
        try (Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession()){
            Transaction tx1 = session.beginTransaction();
            session.delete(group);
            tx1.commit();
        }
    }
}
