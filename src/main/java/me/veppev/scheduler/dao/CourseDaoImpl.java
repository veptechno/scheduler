package me.veppev.scheduler.dao;

import me.veppev.scheduler.entities.Course;
import me.veppev.scheduler.utils.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CourseDaoImpl implements CourseDao {
    @Override
    public Course findById(int id) {
        try (Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession()) {
            return session.get(Course.class, id);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public Set<Course> findAll() {
        try (Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession()) {
            return new HashSet<>((List<Course>) session.createQuery("From Course").list());
        }
    }

    @Override
    public void save(Course course) {
        try (Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession()) {
            Transaction tx1 = session.beginTransaction();
            session.save(course);
            tx1.commit();
        }
    }

    @Override
    public void delete(Course course) {
        try (Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession()) {
            Transaction tx1 = session.beginTransaction();
            session.delete(course);
            tx1.commit();
        }
    }
}
