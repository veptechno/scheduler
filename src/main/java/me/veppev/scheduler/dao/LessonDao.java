package me.veppev.scheduler.dao;

import me.veppev.scheduler.entities.Lesson;

import java.util.Set;

public interface LessonDao {

    void save(Lesson lesson);

    Set<Lesson> findAll();

    Lesson findById(int id);

    void delete(Lesson lesson);
}
