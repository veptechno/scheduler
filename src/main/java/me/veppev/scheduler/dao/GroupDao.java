package me.veppev.scheduler.dao;

import me.veppev.scheduler.entities.Group;

import java.util.Set;

public interface GroupDao {

    Group findById(int id);

    Set<Group> findAll();

    void save(Group group);

    void delete(Group group);

}
