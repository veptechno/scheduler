package me.veppev.scheduler;

import me.veppev.scheduler.entities.Teacher;
import me.veppev.scheduler.services.TeacherService;

public class Main {
    public static void main(String[] args) {
        int id = 1;
        TeacherService teacherService = new TeacherService();
        Teacher teacher = teacherService.findTeacherById(id);
        System.out.println(teacher);
    }
}