package me.veppev.scheduler.filters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/admin/*")
public class AuthFilter implements Filter {

    private static Logger logger;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        logger = LoggerFactory.getLogger(AuthFilter.class);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession(false);

        if (session == null) {
            logger.info("Новый пользователь с ip={}", request.getRemoteHost());
        }

        boolean loggedIn = session != null && session.getAttribute("granted") != null;
        if (loggedIn){
            logger.info("{} разрешена админская панель", request.getRemoteHost());
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            logger.warn("{} запрещена админская панель", request.getRemoteHost());
            response.sendRedirect(request.getContextPath() + "/login.html");
        }
    }

    @Override
    public void destroy() {
    }
}