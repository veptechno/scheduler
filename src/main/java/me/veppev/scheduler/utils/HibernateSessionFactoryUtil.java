package me.veppev.scheduler.utils;

import me.veppev.scheduler.entities.*;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateSessionFactoryUtil {
    private static SessionFactory sessionFactory;

    private HibernateSessionFactoryUtil() {}

    public static void init() {
        getSessionFactory();
    }

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            try {
                Configuration configuration = new Configuration();
                configuration.addAnnotatedClass(Teacher.class);
                configuration.addAnnotatedClass(Course.class);
                configuration.addAnnotatedClass(Group.class);
                configuration.addAnnotatedClass(Lesson.class);

                sessionFactory = configuration.buildSessionFactory(
                        new StandardServiceRegistryBuilder()
                                .applySettings(configuration.getProperties())
                                .build()
                );
            } catch (Exception e) {
                throw e;
            }
        }
        return sessionFactory;
    }

}
