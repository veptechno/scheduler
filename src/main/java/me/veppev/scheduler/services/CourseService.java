package me.veppev.scheduler.services;

import me.veppev.scheduler.dao.CourseDao;
import me.veppev.scheduler.dao.CourseDaoImpl;
import me.veppev.scheduler.entities.Course;

import java.util.List;
import java.util.Set;

public class CourseService {

    private CourseDao courseDao = new CourseDaoImpl();

    public Set<Course> findAll() { return courseDao.findAll(); }

    public Course findCourseById(int id) {
        return courseDao.findById(id);
    }

    public void save(Course course) {
        courseDao.save(course);
    }

    public void delete(Course course) {
        courseDao.delete(course);
    }

}
