package me.veppev.scheduler.services;

import me.veppev.scheduler.dao.TeacherDao;
import me.veppev.scheduler.dao.TeacherDaoImpl;
import me.veppev.scheduler.entities.Course;
import me.veppev.scheduler.entities.Lesson;
import me.veppev.scheduler.entities.Teacher;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class TeacherService {

    private TeacherDao teacherDao = new TeacherDaoImpl();

    public Teacher findTeacherById(int id) {
        return teacherDao.findById(id);
    }

    public void save(Teacher teacher) {
        teacherDao.save(teacher);
    }

    public void update(Teacher teacher) {
        teacherDao.update(teacher);
    }

    public void delete(Teacher teacher) {
        teacherDao.delete(teacher);
    }

    public Set<Teacher> findAll() {
        return teacherDao.findAll();
    }

    public boolean teacherExists(String email, String password) {
        Teacher teacher = teacherDao.findByEmail(email);
        return teacher != null && teacher.getPassword().equals(password);
    }

    public List<Lesson> getLessons(Teacher teacher) {
        return teacher.getCourses().stream().map(Course::getLessons).flatMap(Collection::stream).collect(Collectors.toList());
    }
}
