package me.veppev.scheduler.services;

import me.veppev.scheduler.dao.LessonDao;
import me.veppev.scheduler.dao.LessonDaoImpl;
import me.veppev.scheduler.entities.Lesson;

import java.util.Set;

public class LessonService {

    private LessonDao lessonDao = new LessonDaoImpl();

    public void save(Lesson lesson) {
        lessonDao.save(lesson);
    }

    public Set<Lesson> findAll() {
        return lessonDao.findAll();
    }

    public Lesson findLessonById(int id) {
        return lessonDao.findById(id);
    }

    public void delete(Lesson lesson) {
        lessonDao.delete(lesson);
    }
}
