package me.veppev.scheduler.services;

import me.veppev.scheduler.dao.GroupDao;
import me.veppev.scheduler.dao.GroupDaoImpl;
import me.veppev.scheduler.entities.Group;

import java.util.List;
import java.util.Set;

public class GroupService {

    private GroupDao groupDao = new GroupDaoImpl();

    public Group findGroupById(int id) {
        return groupDao.findById(id);
    }

    public Set<Group> findAll() {
        return groupDao.findAll();
    }

    public void save(Group group) {
        groupDao.save(group);
    }

    public void delete(Group group) { groupDao.delete(group); }
}
