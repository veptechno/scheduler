var app = angular.module("teachersList", []);

app.controller("myCtrl", function($scope, $http) {
    $scope.updateList = function() {
        $http.get('/teacher')
            .then(function(res){
                $scope.teachers = res.data;
            });
    };
    $scope.form = {
        firstName: 'first',
        secondName: 'second',
        email: 'email',
        birthDate: '2010-02-03',
        salary: 23,
        password: 'df'
    };
    $scope.addTeacher = function () {
        $http.post('/teacher', $scope.form)
            .then(function (res) {
                $scope.updateList();
            })
    };
    $scope.deleteTeacher = function (teacher) {
        console.log(teacher.id);
        $http.delete('teacher', {params: {id: teacher.id}})
            .then(function (res) {
                $scope.updateList();
            })
    };
    $scope.updateList();
});

app.directive('teachers', function() {
    return {
        restrict: 'A', // Element directive
        replace: true,
        template: `
<tbody>
  <tr>
  <th>First Name</th>
  <th>Second Name</th>
  <th>Email</th>
  <th>Birth Date</th>
  <th>Salary</th>
  <th>Password</th>
  <th>Delete</th>
</tr>
  <tr ng-repeat="teacher in teachers">
    <td>{{teacher.firstName}}</td>
    <td>{{teacher.secondName}}</td>
    <td>{{teacher.email}}</td>
    <td>{{teacher.birthDate}}</td>
    <td>{{teacher.salary}}</td>
    <td>{{teacher.password}}</td>
    <td><button ng-click="deleteTeacher(teacher)">Del</button></td>
</tr>
</tbody>`
    };
});