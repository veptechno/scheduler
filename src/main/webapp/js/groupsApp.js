let app = angular.module('groupsApp', []);

// Setup controller
app.controller("groupsController", function ($scope, $http) {

    $scope.groups_list = [];
    $scope.new_group_data = {
        number: "",
        year: 2020
    };

    $scope.updateGroupsList = function () {
        let groups_list = [];
        $http({method: 'GET', url: '/group'}).then(
            function successCallback(response) {
                const groups_from_server = angular.fromJson(response.data);
                groups_from_server.forEach(function (group) {
                    groups_list.push({
                        id: group.id,
                        number: group.number,
                        graduation_year: group.year,
                        to_remove: false
                    });
                });
                $scope.groups_list = groups_list;
            }, function errorCallback(response) {
                window.alert('connection to backend failed');
                console.log(response);
            }
        );
    };

    $scope.addGroup = function () {
        const data_to_send = angular.toJson($scope.new_group_data);
        $http({method: 'POST', url: '/group', data: data_to_send}).then(
            function successCallback(res) {
                $scope.updateGroupsList();
            },
            function errorCallback(response) {
                window.alert('group addition rejected by the server');
                console.log(response);
            }
        );
    };

    $scope.removeGroups = function () {
        $scope.groups_list
            .filter(item => item.to_remove === true)
            .forEach(item => $scope.removeGroup(item.id));
    };

    $scope.removeGroup = function (id) {
        $http({method: 'DELETE', url: '/group?id=' + id}).then(
            function successCallback(response) {
                $scope.updateGroupsList();
            },
            function errorCallback(response) {
                window.alert('connection to backend failed');
                console.log(response);
            }
        );
    }

});