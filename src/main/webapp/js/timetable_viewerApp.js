function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

// Retrieve existing module object
var app = angular.module('timetable_viewerApp');

// Setup controller
app.controller("timetable_viewerController", function ($scope, $http) {

    // loading result can be 'fail', 'success' or none if still waiting for the result
    $scope.lessons_list = null;
    $scope.courses_list = null;
    $scope.teachers_list = null;
    $scope.groups_list = null;

    $scope.viewer_state = {option: 'nocond', id: null}; // option can be 'teachers' for teachers-based output, 'groups' for groups-based output or 'nocond' to show all date

    $scope.updateLessonsList = async function () {
        let out_lessons_list = [];
        switch ($scope.viewer_state.option) {
            case 'nocond':
                out_lessons_list = await $scope.getAllLessons();
                break;
            case 'teachers':
                out_lessons_list = await $scope.getLessonsByTeacherId($scope.viewer_state.id);
                break;
            case 'groups':
                out_lessons_list = await $scope.getLessonsByGroupId($scope.viewer_state.id);
                break;
        }
        $scope.$apply(function () {
            $scope.lessons_list = out_lessons_list;
        });
    };

    $scope.loadCoursesList = function () {
        $http({method: 'GET', url: '/course'}).then(
            function successCallback(response) {
                $scope.courses_list = angular.fromJson(response.data);
            },
            function errorCallback(response) {
                window.alert('server rejected addition');
                console.log(response);
            }
        );
    };

    $scope.loadGroupsList = function () {
        $http({method: 'GET', url: '/group'}).then(
            function successCallback(response) {
                $scope.groups_list = angular.fromJson(response.data);
            },
            function errorCallback(response) {
                window.alert('server rejected addition');
                console.log(response);
            }
        );
    };

    $scope.loadTeachersList = async function () {
        await $http({method: 'GET', url: '/teacher'}).then(
            function successCallback(response) {
                $scope.teachers_list = angular.fromJson(response.data);
            },
            function errorCallback(response) {
                window.alert('server rejected addition');
                console.log(response);
            }
        );
    };

    $scope.getAllLessons = async function () {
        return await $scope.getLessonsByUrl('/timetable');
    };

    $scope.getLessonsByTeacherId = async function (id) {
        return await $scope.getLessonsByUrl('/timetable?teacher_id=' + id);
    };

    $scope.getLessonsByGroupId = async function (id) {
        return await $scope.getLessonsByUrl('/timetable?group_id=' + id);
    };

    $scope.getLessonsByUrl = async function (url) {
        let lessons_list = [];
        await $http({method: 'GET', url: url}).then(
            function successCallback(response) {
                lessons_list = angular.fromJson(response.data);
                console.log(lessons_list);
            },
            function errorCallback(response) {
                window.alert('server rejected addition');
                console.log(response);
            }
        );

        await $scope.fillLessonsList(lessons_list);
        console.log(lessons_list);

        return lessons_list;
    };

    $scope.fillLessonsList = async function(lessons_list) {
        for (let i = 0; i < lessons_list.length; i++) {
            let lesson = lessons_list[i];
            lesson.group = await $scope.findGroupById(lesson.group_id);
            lesson.course = await $scope.findCourseById(lesson.course_id);
            lesson.teacher = await $scope.findTeacherById(lesson.course.teacher_id);
        }
    };

    $scope.findGroupById = async function (id) {
        while ($scope.groups_list === null) {
            await sleep(100);
        }

        return $scope.groups_list.find(group => group.id === id);
    };

    $scope.findCourseById = async function (id) {
        while ($scope.courses_list === null) {
            await sleep(100);
        }

        return $scope.courses_list.find(course => course.id === id);
    };

    $scope.findTeacherById = async function (id) {
        if ($scope.teachers_list === null) {
            await $scope.loadTeachersList();
        }

        return $scope.teachers_list.find(teacher => teacher.id === id);
    };

    $scope.$watch("viewer_state.option", function (newValue, oldValue) {
        if (newValue === 'nocond') {
            $scope.updateLessonsList();
        } else if (newValue === 'teachers') {
            $scope.viewer_state.id = $scope.teachers_list.length > 0 ? $scope.teachers_list[0].id : -1;
        } else {
            $scope.viewer_state.id = $scope.groups_list.length > 0 ? $scope.groups_list[0].id : -1;
        }
    });

    $scope.$watch("viewer_state.id", function (newValue, oldValue) {
        if (newValue !== -1) {
            $scope.updateLessonsList();
        }
    });

});