// Retrieve existing module object
let app = angular.module('teachersApp', []);

// Setup controller
app.controller("teachersController", function ($scope, $http) {

    $scope.teachers_list = [];
    $scope.new_teacher_data = {
		firstName: "",
		secondName: "",
		salary: 0,
		birthDate: new Date(),
		email: "",
		password: "",
	};

    $scope.updateTeachersList = function () {
    	let teachers_list = [];
        $http({method: 'GET', url: '/teacher'}).then(
            function successCallback(response) {
                const teachers = angular.fromJson(response.data);
                teachers.forEach(function (item) {
                    teachers_list.push(
                        {
                            id: item.id,
                            firstName: item.firstName,
                            secondName: item.secondName,
                            salary: item.salary,
                            birthDate: new Date(item.birthDate),
                            email: item.email,
							password: item.password,
                            to_remove: false
                        });
                });
				$scope.teachers_list = teachers_list;
            },
            function errorCallback(response) {
                window.alert('Connection to backend failed');
                console.log(response);
            }
        );
    };

    $scope.addTeacher = function () {
        const data_to_send = angular.toJson($scope.new_teacher_data);
        $http({method: 'POST', url: '/teacher', data: data_to_send}).then(
            function successCallback(response) {
                $scope.updateTeachersList();
            },
            function errorCallback(response) {
                window.alert('server rejected addition');
				console.log(response);
            }
        );
    };

    $scope.removeTeachers = function () {
        $scope.teachers_list
            .filter(item => item.to_remove === true)
            .forEach(item => $scope.removeTeacher(item.id));
    };

    $scope.removeTeacher = function (id) {
		$http({method: 'DELETE', url: '/teacher?id=' + id}).then(
			function successCallback(response) {
				$scope.updateTeachersList();
			},
			function errorCallback(response) {
				window.alert('connection to backend failed');
				console.log(response);
			}
		);
	}
});