// Retrieve existing module object
let app = angular.module('lessonsApp', []);

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

// Setup controller
app.controller("lessonsController", function ($scope, $http) {

    $scope.lessons_list;

    $scope.courses_list = null;
    $scope.groups_list = null;

    $scope.new_lesson_data = {
        group_id: -1,
        course_id: -1,
        day: 0,
        time: "14:00"
    };


    $scope.loadGroupsList = function () {
        $http({method: 'GET', url: '/group'}).then(
            function successCallback(response) {
                $scope.groups_list = angular.fromJson(response.data);
            }, function errorCallback(response) {
                window.alert('connection to backend failed');
                console.log(response);
                $scope.groups_list = [];
            }
        );
    };

    $scope.loadCoursesList = function () {
        $http({method: 'GET', url: '/course'}).then(
            function successCallback(response) {
                $scope.courses_list = angular.fromJson(response.data);
            }, function errorCallback(response) {
                window.alert('connection to backend failed');
                console.log(response);
                $scope.groups_list = [];
            }
        );
    };

    $scope.updateLessonsList = async function () {
        $scope.lessons_list = [];

        $http({method: 'GET', url: '/lesson'}).then(
            function successCallback(response) {
                const lessons_from_server = angular.fromJson(response.data);
				lessons_from_server.forEach(async function (lesson) {
                    let group = await $scope.findGroupById(lesson.group_id);
                    let course = await $scope.findCourseById(lesson.course_id);
                    $scope.$apply(function () {
                        $scope.lessons_list.push({
                            id: lesson.id,
                            day: lesson.day,
                            time: lesson.time,
                            group_number: group.number,
                            course_title: course.title,
                            to_remove: false
                        });
                    });
                });
            }, function errorCallback(response) {
                console.log('connection to backend failed');
            });
    };

    $scope.findGroupById = async function (id) {
        while ($scope.groups_list === null) {
            await sleep(100);
        }

        return $scope.groups_list.find(group => group.id === id);
    };

    $scope.findCourseById = async function (id) {
        while ($scope.courses_list === null) {
            await sleep(100);
        }

        return $scope.courses_list.find(course => course.id === id);
    };

    $scope.addLesson = function () {
        const data_to_send = angular.toJson($scope.new_lesson_data);
        console.log($scope.new_lesson_data);
        $http({method: 'POST', url: '/lesson', data: data_to_send}).then(
            function successCallback(res) {
                $scope.updateLessonsList();
            },
            function errorCallback(response) {
                window.alert('lesson addition rejected by the server');
                console.log(response);
            }
        );
    };

    $scope.removeLessons = function () {
        $scope.lessons_list
            .filter(item => item.to_remove === true)
            .forEach(item => $scope.removeLesson(item.id));
    };

    $scope.removeLesson = function (id) {
        $http({method: 'DELETE', url: '/lesson?id=' + id}).then(
            function successCallback(response) {
                $scope.updateLessonsList();
            },
            function errorCallback(response) {
                window.alert('connection to backend failed');
                console.log(response);
            }
        );
    }
});