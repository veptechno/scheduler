let app = angular.module('coursesApp', []);

// Setup controller
app.controller("coursesController", function ($scope, $http) {

    $scope.courses_list = [];
    $scope.teachers_list = null;
    $scope.new_course_data = {
    	title: "",
		teacher_id: -1
	};

    // called once
    $scope.loadTeachersList = async function () {
        let teachers_list = [];
        await $http({method: 'GET', url: '/teacher'}).then(
            function successCallback(response) {
                const teachers_json = angular.fromJson(response.data);
                teachers_json.forEach(item => {
                    teachers_list.push({
                        id: item.id,
                        name: item.firstName.concat(' ', item.secondName)
                    });
                });
                $scope.teachers_list = teachers_list;
            }, function errorCallback(response) {
                window.alert('connection to backend failed');
                console.log(response);
            }
        );
    };

    $scope.updateCoursesList = async function () {
        $http({method: 'GET', url: '/course'}).then(
            function successCallback(response) {
                const courses_from_server = angular.fromJson(response.data);
                $scope.courses_list = [];

                courses_from_server.forEach(async function (course) {
                    let teacher = await $scope.findTeacherById(course.teacher_id);

                    $scope.$apply(function () {
                        $scope.courses_list.push({
                            id: course.id,
                            title: course.title,
                            teacher_name: teacher === undefined ? "Undefined name" : teacher.name
                        });
                    });
                });
            }, function errorCallback(response) {
                window.alert('connection to backend failed');
                console.log(response);
            }
        );
    };

    $scope.findTeacherById = async function (id) {
        if ($scope.teachers_list === null) {
            await $scope.loadTeachersList();
        }

        return $scope.teachers_list.find(teacher => teacher.id === id);
    };

    $scope.addCourse = function () {
        const data_to_send = angular.toJson($scope.new_course_data);
        $http.post('/course', data_to_send).then(
            function successCallback(res) {
                $scope.updateCoursesList();
            },
            function errorCallback(response) {
                window.alert('course addition rejected by the server');
				console.log(response);
            }
        );
    };

    $scope.removeCourses = function () {
        $scope.courses_list
			.filter(item => item.to_remove === true)
			.forEach(item => $scope.removeCourse(item.id));
    };

    $scope.removeCourse = function (id) {
        $http({method: 'DELETE', url: '/course?id=' + id}).then(
            function successCallback(response) {
                $scope.updateCoursesList();
            },
            function errorCallback(response) {
                window.alert('connection to backend failed');
				console.log(response);
            }
        );
    };
});