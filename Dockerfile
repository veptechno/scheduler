FROM jetty:9

    COPY ./build/libs/scheduler.war /var/lib/jetty/webapps/ROOT.war

RUN ["mkdir", "/var/lib/jetty/logs/"]

CMD ["java","-jar","/usr/local/jetty/start.jar"]